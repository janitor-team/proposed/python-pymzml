#!/bin/sh

topSrcDir="$PWD"

if [ ! -d "debian" ]
then 
    echo "Not located in the top source directory."
    exit 1
fi

# printf "${topSrcDir}\n"

# Remove the old file, that is, we overwrite it now.
rm -vf "${topSrcDir}/debian/source/include-binaries"

cd "${topSrcDir}/debian/python-pymzml-doc/usr/share/doc/python3-pymzml/html" || exit 1


for item in $(find -type f)
do
    fileType=$(file -b ${item} | awk '{print $1}')

    if [ "${fileType}" = "PNG" ] || \
           [ "${fileType}" = "GIF" ] || \
           [ "${fileType}" = "PDF" ] || \
           [ "${fileType}" = "data" ]
    then
        echo "debian/python-pymzml-doc/usr/share/doc/python3-pymzml/html/${item}" >> ${topSrcDir}/debian/source/include-binaries
    fi

done

# Finally add the pdf file itself:
echo "debian/python-pymzml-doc/usr/share/doc/python3-pymzml/pymzML.pdf" >> ${topSrcDir}/debian/source/include-binaries
