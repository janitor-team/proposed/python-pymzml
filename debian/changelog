python-pymzml (2.5.2+repack1-1) unstable; urgency=low

  * New upstream version.

  * Add tex-gyre doc build-deps for
    /usr/share/texmf/tex/latex/tex-gyre/tgtermes.sty.

  * Add imagemagick-6-common as build-deps for mapping the missing
    n019003l.pfb font required by pdflatex to build the doc to the
    /usr/share/fonts/X11/Type1/NimbusSans-Regular.pfb file. That fixes the
    problem.

 -- Filippo Rusconi <lopippo@debian.org>  Sun, 01 Jan 2023 21:35:08 +0100

python-pymzml (2.4.7-3) unstable; urgency=low

  * Fix upgrade testing->sid failure (Closes: #963190). Thanks , Andreas
    Beckmann <anbe@debian.org>, for the report.

 -- Filippo Rusconi <lopippo@debian.org>  Sun, 21 Jun 2020 10:36:22 +0200

python-pymzml (2.4.7-2) unstable; urgency=low

  * Fix documentation build problem with svg files around 
    (Closes: #962928). This build failure only shows when using sbuild and not
    gbp buildpackage. Apparently, it only shows when the doc-building process
    fails to access the network.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 16 Jun 2020 09:14:42 +0200

python-pymzml (2.4.7-1) unstable; urgency=low

  * New upstream version.

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 15 Jun 2020 15:34:23 +0200

python-pymzml (2.1.0-1) unstable; urgency=low

  * New upstream version.

 -- Filippo Rusconi <lopippo@debian.org>  Fri, 08 Mar 2019 12:00:48 +0100

python-pymzml (2.0.0-1) unstable; urgency=low

  * Fix FTBFS with Sphinx 1.6: Needs build-dep on latexmk,
  * thanks to Dmitry Shachnev <mitya57@debian.org> and Steve Langasek
    <steve.langasek@canonical.com> for reporting and help.
  * d/control: Standards-Version: 4.3.0
  * d/control: fix Vcs fields.

 -- Filippo Rusconi <lopippo@debian.org>  Fri, 08 Mar 2019 11:26:28 +0100

python-pymzml (20170926-1) unstable; urgency=low

  * New upstream release from the git repos (master pymzML branch).

  * Add dependency on latexmk for building the Sphinx-based documentation
    (Closes: #872242: python-pymzml: FTBFS with Sphinx 1.6: Needs build-dep on
    latexmk ; thanks to Dmitry Shachnev <mitya57@debian.org> for reporting
    it).

  * Add dependency on python-sphinx-rtd-theme since that package is no
    longer a hard dependency for python-sphinx.

  * Abandon old patch about removing the shebang line from py files since 
    upstream implemented my suggestion.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 26 Sep 2017 09:54:13 +0200

python-pymzml (0.7.6-dfsg-5) unstable; urgency=low

  * debian/control: update the dependencies latex-xcolor replaced with
    texlive-latex-recommended (bug number-865237 was closed by non-maintainer
    upload by Adrian Bunk <bunk@debian.org> Tue, 18 Jul 2017 19:50:54 +0300.

  * debian/compat: 9 (update dep to debhelper (>=9.20130630)).

  * Standards-Version: 4.0.1 (no changes required).

 -- Filippo Rusconi <lopippo@debian.org>  Fri, 15 Sep 2017 14:05:47 +0200

python-pymzml (0.7.6-dfsg-4) unstable; urgency=low

  * debian/control: update the name of the git repository to match the new
    name of the package.
  * debian/control: set secure https protocol also for the vcs-git url.

 -- Filippo Rusconi <lopippo@debian.org>  Thu, 01 Sep 2016 10:43:36 +0200

python-pymzml (0.7.6-dfsg-3) unstable; urgency=low

  * Fix problem with debian/control Replaces/Conflicts directives. Now
    python-mzml should be properly removed from the system upon installation
    of this replacing package.

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 29 Aug 2016 14:12:31 +0200

python-pymzml (0.7.6-dfsg-2) unstable; urgency=low

  * Fix bug reported by Lucas Nussbaum <lucas@debian.org> about iftex.sty not
    being found (add a texlive dependency to d/control). Closes: #830331.
  * Minor fixes to the debian packaging.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 19 Jul 2016 16:23:38 +0200

python-pymzml (0.7.6-dfsg-1) unstable; urgency=medium

  * New upstream version that introduces setup.py as a means of building
    and installing the software.
  * d/rules: change build system to include Python3.
  * New patch to remove the shebang line from all the class-defining
    Python files where it is not needed.
  * d/control: rename package names from python-mzml to python-pymzml to
    better stick to the general convention in the Debian archive.

 -- Filippo Rusconi <lopippo@debian.org>  Thu, 26 Nov 2015 10:19:46 +0100

python-mzml (0.7.4-dfsg-3) unstable; urgency=medium

  * mv upstream upstream/metadata;
  
  * d/control: Canonicalize the VCS uris;
  
  * d/control: set Standards-Version: 3.9.5
  
 -- Filippo Rusconi <lopippo@debian.org>  Wed, 20 Aug 2014 14:18:01 +0200

python-mzml (0.7.4-dfsg-2) unstable; urgency=low

  * python-mzml.install: change the usr/lib/python2.[67]/dist-packages
    install directories to the degenerate form
    usr/lib/python2.*/dist-packages/pymzml. The package will still build
    and install fine when 2.6 is removed. Thanks to Dmitry Shachnev
    <mitya57@gmail.com> for suggesting the fix. Closes: #697189.

 -- Filippo Rusconi <lopippo@debian.org>  Wed, 02 Jan 2013 20:38:47 +0100

python-mzml (0.7.4-dfsg-1) unstable; urgency=low

  * Initial packaging of the pymzML software with COPYING file shipped for
    the first time within the source code tree after request to
    upstream. Closes: #690177.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 09 Oct 2012 22:38:41 +0200
