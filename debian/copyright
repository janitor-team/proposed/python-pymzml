Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pymzML
Upstream-Contact: Dr. Christian Fufezan (christian@fufezan.net)
Source: http://pymzml.github.com/dist/pymzml.tar.bz2
Comment: A number of files were removed from the upstream source tarball, in
 particular all the git and other development tools files (.gitignore,
 appveyor.xml...). Also the test data files that were present in both
 uncompressed and compressed gzipped form were removed (only the gzipped version
 was retained).

Files: *
Copyright: 2010-2020 M. Kösters,
 J. Leufken,
 T. Bald,
 A. Niehues,
 S. Schulze,
 K. Sugimoto,
 R.P. Zahedi,
 M. Hippler,
 S.A. Leidel,
 C. Fufezan (christian@fufezan.net)
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 . 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 . 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: debian/*
Copyright: 2012 Filippo Rusconi (lopippo@debian.org)
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-3'.
 
